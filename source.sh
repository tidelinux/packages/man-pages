# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://www.kernel.org/pub/linux/docs/man-pages/man-pages-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  echo "$DOWNLOAD"
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
  rm "$BPM_SOURCE"/man3/crypt*
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make prefix=/usr DESTDIR="$BPM_OUTPUT" install
}
